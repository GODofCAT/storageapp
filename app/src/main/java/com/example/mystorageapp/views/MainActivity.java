package com.example.mystorageapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mystorageapp.R;
import com.example.mystorageapp.controllers.ControllerMainActivity;

public class MainActivity extends AppCompatActivity {

    private ControllerMainActivity controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controller = new ControllerMainActivity(this);
        controller.UpdateListViewThingsWithOutFilters();

        EditText editTextName = findViewById(R.id.editTextName);


        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                controller.UpdateListViewThingsWithFilters();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void onButtonReloadThingsClick(View view)
    {
        controller.ReloadThings();
        controller.UpdateListViewThingsWithOutFilters();

        Toast.makeText(getApplicationContext(),"Things reloaded",Toast.LENGTH_SHORT).show();
    }

    public void onButtonLogOutClick(View view){
        controller.LogOut();
    }
}