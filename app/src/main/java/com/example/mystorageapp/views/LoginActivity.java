package com.example.mystorageapp.views;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.mystorageapp.R;
import com.example.mystorageapp.controllers.ControllerLoginActivity;

public class LoginActivity extends AppCompatActivity {
    private ControllerLoginActivity controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        controller = new ControllerLoginActivity(this);
    }

    public  void onButtonJoinClick(View view){
        controller.Join();
    }public  void onButtonExitClick(View view){
        controller.Exit();
    }
}
