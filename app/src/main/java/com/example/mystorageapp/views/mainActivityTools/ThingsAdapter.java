package com.example.mystorageapp.views.mainActivityTools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mystorageapp.model.entities.Thing;

import java.util.ArrayList;

public class ThingsAdapter extends BaseAdapter {
    private ArrayList<Thing> things;

    Context context;
    LayoutInflater layoutInflater;

    public ThingsAdapter(Context context, ArrayList<Thing> things)
    {
        this.context = context;
        this.things = things;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return things.size();
    }

    @Override
    public Object getItem(int i) {
        return things.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if(v == null)
        {
            v = layoutInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        }

        Thing thing = things.get(i);

        TextView textView = v.findViewById(android.R.id.text1);

        String str = String.format("Name: %s\nCount: %s",thing.getCount(), thing.getName());

        textView.setText(str);

        return v;
    }
}
