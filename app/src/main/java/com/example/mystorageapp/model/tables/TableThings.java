package com.example.mystorageapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mystorageapp.model.entities.Thing;
import com.example.mystorageapp.model.tools.DbHelper;

import java.util.ArrayList;

public class TableThings {
    DbHelper dbHelper;

    public TableThings(DbHelper dbHelper){this.dbHelper = dbHelper;}

    public ArrayList<Thing> getAll()
    {
        ArrayList<Thing> things = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `things`";

        Cursor cursor = db.rawQuery(sqlCommand, null);

        while (cursor.moveToNext() == true)
        {
            Thing thing = new Thing(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );

            things.add(thing);
        }

        cursor.close();

        dbHelper.close();

        return things;
    }
}
