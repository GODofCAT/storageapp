package com.example.mystorageapp.model;

import android.content.Context;

import com.example.mystorageapp.model.tables.TableThings;
import com.example.mystorageapp.model.tables.TableWorker;
import com.example.mystorageapp.model.tools.DbHelper;

public class DbManager {
    private static DbManager instance = null;

    public static DbManager getInstance(Context context) {
        if (instance==null){
            instance = new DbManager(context);
        }
        return instance;
    }

    private TableThings tableThings;
    private TableWorker tableWorker;

    private DbManager(Context context){
        DbHelper dbHelper = new DbHelper(context);

        tableThings = new TableThings(dbHelper);
        tableWorker = new TableWorker(dbHelper);
    }

    public TableWorker getTableWorkers() {
        return tableWorker;
    }

    public TableThings getTableThings() {
        return tableThings;
    }
}
