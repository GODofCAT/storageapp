package com.example.mystorageapp.model.entities;

public class Thing {
    int id;
    String count;
    String name;

    public Thing(int id, String string, String count, String name){
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public String getCount() {
        return count;
    }

    public String getName() {
        return name;
    }
}
