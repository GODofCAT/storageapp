package com.example.mystorageapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.mystorageapp.model.entities.Worker;
import com.example.mystorageapp.model.tools.DbHelper;

public class TableWorker {
    DbHelper dbHelper;

    public  TableWorker(DbHelper dbHelper){this.dbHelper = dbHelper;}

    public Worker getByLoginAndPassword(String login, String password)
    {
        Worker worker = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = String.format("SELECT * FROM `workers` WHERE `login`='%s' AND `password`='%s'", login, password);

        Cursor cursor = db.rawQuery(sqlCommand, null);

        if(cursor.moveToFirst()==true)
        {
            worker = new Worker(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );
        }

        cursor.close();

        dbHelper.close();

        return worker;
    }
}
