package com.example.mystorageapp.controllers;

import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mystorageapp.R;
import com.example.mystorageapp.model.DbManager;
import com.example.mystorageapp.model.entities.Thing;
import com.example.mystorageapp.model.entities.Worker;
import com.example.mystorageapp.model.tools.DataStorage;
import com.example.mystorageapp.views.MainActivity;
import com.example.mystorageapp.views.mainActivityTools.ThingsAdapter;

import java.util.ArrayList;

public class ControllerMainActivity  {
    private MainActivity mainActivity;
    private DbManager db;

    private ArrayList<Thing> things;

    public ControllerMainActivity(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;

        db = DbManager.getInstance(this.mainActivity.getApplicationContext());

        things = db.getTableThings().getAll();

        TextView textView = this.mainActivity.findViewById(R.id.textViewWorker);

        Worker worker = (Worker) DataStorage.Get("worker");

        textView.setText("Welcome, "+worker.getName());
    }

    private void UpdateListViewThings(ArrayList<Thing> passangers)
    {
        ThingsAdapter adapter = new ThingsAdapter(mainActivity.getApplicationContext(), passangers);

        ListView listView = mainActivity.findViewById(R.id.listViewThings);

        listView.setAdapter(adapter);
    }

    public void UpdateListViewThingsWithOutFilters(){
        UpdateListViewThings(things);
    }

    public void UpdateListViewThingsWithFilters(){
        ArrayList<Thing> filterThings = new ArrayList<>();

        EditText editText = mainActivity.findViewById(R.id.editTextName);
        String count = editText.getText().toString();

        for (int i = 0; i < things.size(); i++) {
            if (things.get(i).getName().startsWith(count)==true){
                filterThings.add(things.get(i));
            }
        }

        UpdateListViewThings(things);
    }

    public void ReloadThings(){
        things = db.getTableThings().getAll();
    }

    public void LogOut(){
        DataStorage.Delete("worker");
        mainActivity.finish();
    }
}
