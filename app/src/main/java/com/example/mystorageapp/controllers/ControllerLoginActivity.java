package com.example.mystorageapp.controllers;

import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mystorageapp.R;
import com.example.mystorageapp.model.DbManager;
import com.example.mystorageapp.model.entities.Worker;
import com.example.mystorageapp.model.tools.DataStorage;
import com.example.mystorageapp.views.LoginActivity;
import com.example.mystorageapp.views.MainActivity;

public class ControllerLoginActivity {

    private LoginActivity loginActivity;
    private DbManager db;

    private EditText editTextTextLogin,editTextTextPassword;
    private Button buttonLogin;

    public ControllerLoginActivity(LoginActivity loginActivity){
        editTextTextLogin = loginActivity.findViewById(R.id.editTextTextLogin);
        editTextTextPassword = loginActivity.findViewById(R.id.editTextTextPassword);

        buttonLogin = loginActivity.findViewById(R.id.buttonLogin);

    }

    public void Join(){
        EditText editTextLogin, editTextPassword;
        Context context;

        editTextLogin = loginActivity.findViewById(R.id.editTextTextLogin);
        editTextPassword = loginActivity.findViewById(R.id.editTextTextPassword);
        context = loginActivity.getApplicationContext();

        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        if(login.equals("")==true)
        {
            Toast.makeText(context,"Error. Login is empty",Toast.LENGTH_SHORT).show();
            return;
        }

        if(password.equals("")==true)
        {
            Toast.makeText(context,"Error. Password is empty",Toast.LENGTH_SHORT).show();
            return;
        }

        Worker worker = db.getTableWorkers().getByLoginAndPassword(login, password);

        if(worker==null)
        {
            Toast.makeText(context,"Error. Login or Password is wrong",Toast.LENGTH_SHORT).show();
        }
        else
        {
            DataStorage.Add("worker", worker);
            editTextLogin.getText().clear();
            editTextPassword.getText().clear();

            Intent intent = new Intent(context, MainActivity.class);
            loginActivity.startActivity(intent);
        }
    }

    public  void Exit(){
        loginActivity.finish();
    }
}
